import { glassesList } from './glassesList.js'
import { glasses } from './glasses.js';

let dataGlasses = [
    { id: "G1", src: "./img/g1.jpg", virtualImg: "./img/v1.png", brand: "Armani Exchange", name: "Bamboo wood", color: "Brown", price: 150, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? " },
    { id: "G2", src: "./img/g2.jpg", virtualImg: "./img/v2.png", brand: "Arnette", name: "American flag", color: "American flag", price: 150, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G3", src: "./img/g3.jpg", virtualImg: "./img/v3.png", brand: "Burberry", name: "Belt of Hippolyte", color: "Blue", price: 100, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G4", src: "./img/g4.jpg", virtualImg: "./img/v4.png", brand: "Coarch", name: "Cretan Bull", color: "Red", price: 100, description: "In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G5", src: "./img/g5.jpg", virtualImg: "./img/v5.png", brand: "D&G", name: "JOYRIDE", color: "Gold", price: 180, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?" },
    { id: "G6", src: "./img/g6.jpg", virtualImg: "./img/v6.png", brand: "Polo", name: "NATTY ICE", color: "Blue, White", price: 120, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G7", src: "./img/g7.jpg", virtualImg: "./img/v7.png", brand: "Ralph", name: "TORTOISE", color: "Black, Yellow", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam." },
    { id: "G8", src: "./img/g8.jpg", virtualImg: "./img/v8.png", brand: "Polo", name: "NATTY ICE", color: "Red, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim." },
    { id: "G9", src: "./img/g9.jpg", virtualImg: "./img/v9.png", brand: "Coarch", name: "MIDNIGHT VIXEN REMIX", color: "Blue, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet." }
];


let glist = new glassesList()

const getEle = id => document.getElementById(id)

const danhSachKinh = () => {
    let dsKinh = getEle('vglassesList')
    dataGlasses.map((g, index) => {
        let gl = new glasses(g.id, g.src, g.virtualImg, g.brand, g.name, g.color, g.price, g.description)
        glist.themKinh(gl)
    })
    dsKinh.innerHTML = glist.danhSachKinh()
}

danhSachKinh();

const thuKinh = (event) => {
    console.log(event)
    let id = event.target.getAttribute('id')
    showInfo(glist.dsKinh[id])
}


const showInfo = (kinh) => {
    let avatar = getEle('avatar'),
    info = getEle('glassesInfo')
    avatar.innerHTML = `
    <img id='kinh' src='${kinh.vi}'/>
    `
    
    info.innerHTML = `
    <h4>${kinh.name} - ${kinh.brand} (${kinh.color})</h4>
    <span class="btn btn-danger btn-sm mr-3">$${kinh.price}</span>
    <span class="text-success">Stocking</span>
    <p class="card-text"> ${kinh.des}</p>
    `
    info.style.display = 'block'
}
window.thuKinh = thuKinh

const removeGlasses = (show) => {
    let kinh = getEle('kinh')
    if (kinh == null) {
        return;
    }
    if (show)
        kinh.style.display = 'block'
    else
        kinh.style.display = 'none'
}

window.removeGlasses = removeGlasses



