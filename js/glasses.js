export class glasses{
    constructor(id,src,vi,brand,name,color,price,des){
        this.id = id
        this.src = src
        this.vi = vi
        this.brand = brand
        this.name = name
        this.color = color
        this.price = price
        this.des = des
    }
}